Mgcs_RemoveCustomerLinks
========================

Remove customer links in the Customer Account navigation and change their positions.

Facts
-----

* Module name: Mgcs_RemoveCustomerLinks
* Latest version: See the config.xml (or repository's tags)
* Author: Mark van der Sanden / Magentics
* Magento Connect 1.0 extension key: n/a
* Magento Connect 2.0 extension key: n/a

Description
-----------

By default, Magento lacks the ability to remove links from the Customer Account navigation. Links get added by name, but can't be removed by name. I think that every frontend developer has come across this issue and this is just another module to solve the issue.

Also, I once encountered a situation where I wanted to change the sort order of the links. By default, you cannot define the sort order for a link. I added a way to change the position of links in a safe way: if you uninstall this module, nothing breaks (of course, the links appear at their original position again).

Compatibility
-------------

* Magento CE 1.6+
* Magento EE 1.11+

Installation
------------

* Use [Modman](https://github.com/colinmollenhour/modman)

Usage
-----

See this example. You can add it to your theme's `local.xml` to see it in action:

````xml
<?xml version="1.0"?>
<layout version="0.1.0">
    <customer_account>
        <reference name="customer_account_navigation">
            <!-- remove the 'account edit' link -->
            <action method="removeLinkByName"><link>account_edit</link></action>
            <!-- move the 'address book' link to after 'my orders' -->
            <action method="setLinkPosition"><link>address_book</link><after>orders</after></action>
        </reference>
    </customer_account>
</layout>
````

Uninstallation
--------------

1. Remove all extension files from your Magento installation

Support
-------

If you have any issues with this extension, open an issue on [Bitbucket](https://bitbucket.org/magentics/mgcs_removecustomerlinks)

Contribution
------------

Any contribution is highly appreciated. The best way to contribute code is to open a [pull request on Bitbucket](https://www.atlassian.com/git/tutorials/making-a-pull-request).

Lead Developers
---------------

Mark van der Sanden http://www.magentics.nl/ [@magentics_nl](https://twitter.com/magentics_nl)

License
-------

[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)
