<?php

class Mgcs_RemoveCustomerLinks_Block_Customer_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{
    protected $_positions = array();

    public function removeLinkByName($name)
    {
        if (isset($this->_links[$name])) {
            unset($this->_links[$name]);
        }

        return $this;
    }

    public function setLinkPosition($name, $reference, $after = true)
    {
        $this->_positions[] = array(
            'name'      => $name,
            'reference' => $reference,
            'after'     => (bool)$after
        );
        return $this;
    }

    public function getLinks()
    {
        $links = $this->_links;
        // first, initialize all positions on the links
        $i = 10;
        foreach ($links as $name => $link) {
            $link->setPosition($i);
            $i += 10;
        }
        // then, set the relative positions
        foreach ($links as $name => $link) {
            foreach ($this->_positions as $pos) {
                if ($pos['reference'] == $name && $pos['after'] == false && !empty($links[$pos['name']])) {
                    $links[$pos['name']]->setPosition($link->getPosition() - 1);
                }
                if ($pos['reference'] == $name && $pos['after'] == true && !empty($links[$pos['name']])) {
                    $links[$pos['name']]->setPosition($link->getPosition() + 1);
                }
            }
        }
        // now all positions have been set on the objects: sort it based on the position data
        uasort($links, function($obj1, $obj2) {
            if ($obj1->getPosition() == $obj2->getPosition()) {
                return 0;
            }
            return $obj1->getPosition() > $obj2->getPosition() ? 1 : -1;
        });
        return $links;
    }

}